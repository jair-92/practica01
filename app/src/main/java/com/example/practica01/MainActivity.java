package com.example.practica01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnPulsar;
    private EditText txtNombre;
    private TextView lblSaludar;
    private Button btnLimpiar;
    private Button btnSalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //relacionar los objetos
        btnPulsar = (Button) findViewById(R.id.btnSaludar);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        lblSaludar = (TextView) findViewById(R.id.lblSaludo);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        btnPulsar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //validar

                if(txtNombre.getText().toString().matches("")) {



                    Toast.makeText(MainActivity.this,
                            "Falto capturar informacion",
                            Toast.LENGTH_SHORT).show();
                }
                else {

                    String str = "hola "
                            + txtNombre.getText().toString() +
                            " como estas";
                    lblSaludar.setText(str.toString());
                }
            }
        });

        // Codificar el evento clic del boton Limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Validar el boton
                if(lblSaludar.getText().toString().matches(":: ::")){
                    Toast.makeText(MainActivity.this, "La etiqueta esta vacia", Toast.LENGTH_SHORT).show();
                }
                else{
                    String str = ":: ::";
                    lblSaludar.setText(str.toString());
                }
            }
        });

        // Codificar el evento del boton Salir
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }
}